import list_service
import user_service

from flask import Flask, request, jsonify

# initialisiere Flask-Server
app = Flask(__name__)


# add some headers to allow cross origin access to the API on this server, necessary for using preview in Swagger Editor!
@app.after_request
def apply_cors_header(response):
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'GET,POST,DELETE'
    response.headers['Access-Control-Allow-Headers'] = 'Content-Type'
    return response


# post: creates a new list based on request body
# get: returns a list of all lists.
@app.route('/list', methods=['POST', 'GET'])
def create_or_get_list():
    if request.method == 'POST':
        created_list = jsonify(list_service.createList(request.json))
        if created_list is not None:
            return created_list, 200
        else:
            return 'error while creating list', 404

    if request.method == 'GET':
        lists = jsonify(list_service.getLists())
        if lists is not None:
            return jsonify(list_service.getLists()), 200
        else:
            return 'error while getting lists', 404


@app.route('/list/<list_id>', methods=['GET', 'DELETE'])
def get_or_delete_list_by_id(list_id):
    if request.method == 'GET':
        get_result = list_service.getListById(list_id)
        if 'id' not in get_result:
            return get_result, 404
        else:
            return get_result, 200
    if request.method == 'DELETE':
        delete_result = list_service.deleteListById(list_id)
        if 'id' not in delete_result:
            return delete_result, 404
        else:
            return delete_result, 200


@app.route('/list/<list_id>/entry', methods=['POST', 'DELETE'])
def create_entry(list_id):
    if request.method == 'POST':
        return list_service.createEntry(list_id, request.json)


@app.route('/list/<list_id>/entry/<entry_id>', methods=['POST', 'DELETE'])
def update_or_delete_entry(list_id, entry_id):
    if request.method == 'POST':
        return list_service.updateEntry(list_id, entry_id, request.json)
    if request.method == 'DELETE':
        return list_service.deleteEntry(list_id, entry_id)


@app.route('/user', methods=['POST'])
def create_user():
    return user_service.createUser(request.json)


@app.route('/user/<user_id>', methods=['DELETE'])
def delete_user(user_id):
    return user_service.deleteUser(user_id)


@app.route('/users', methods=['GET'])
def get_all_users():
    return jsonify(user_service.getUsers())


if __name__ == '__main__':
    # starte Flask-Server
    app.run(host='127.0.0.1', port=5000)
