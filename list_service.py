import json
import uuid

import user_service


def getLists():
    with open('inMemoryData/todoLists.json', 'r') as listsJson:
        current_lists = json.load(listsJson)
    if current_lists is not None:
        return current_lists
    else:
        return None


def updateLists(lists):
    with open('inMemoryData/todoLists.json', 'w') as jsonList:
        json.dump(lists, jsonList)


def createList(list_object):
    lists = getLists()
    new_list_ingredients = {'id': str(uuid.uuid4()), 'entries': []}
    new_list = list_object | new_list_ingredients
    lists.append(new_list)
    updateLists(lists)
    if new_list is not None:
        return new_list
    else:
        return None


def deleteListById(list_id):
    lists = getLists()
    for todolist in lists:
        if todolist['id'] == list_id:
            lists.remove(todolist)
            updateLists(lists)
            return todolist
    return 'list not found'


def getListById(list_id):
    lists = getLists()
    for todo_list in lists:
        if todo_list['id'] == list_id:
            return todo_list
    return 'list not found'


def createEntry(list_id, entry_to_create):
    # loads lists and users
    lists = getLists()
    users = user_service.getUsers()
    # saves user id for later comparison
    user_id_to_append = entry_to_create['user']
    new_entry = None
    for todo_list in lists:
        if todo_list['id'] == list_id:
            new_entry_ingredients = {'id': str(uuid.uuid4())}
            # find user by Id and append it to entry object.
            for user in users:
                if user['id'] == entry_to_create['user']:
                    entry_to_create['user'] = user
            # if entry.user has not changed to the requested user, return error
            if entry_to_create['user'] == user_id_to_append:
                return 'user not found'
            # appends generated id to entry
            new_entry = entry_to_create | new_entry_ingredients
            # adds todolist entries
            todo_list['entries'].append(new_entry)
    # updates in memory data
    updateLists(lists)
    if new_entry is not None:
        return new_entry
    else:
        return 'list not found'


def deleteEntry(list_id, entry_id):
    todo_lists = getLists()
    for todo_list in todo_lists:
        if todo_list['id'] == list_id:
            entries = todo_list['entries']
            for entry in entries:
                if entry['id'] == entry_id:
                    entries.remove(entry)
                    updateLists(todo_lists)
                    return entry
            return 'entry not found'
    return 'list not found'


def updateEntry(list_id, entry_id, updated_entry):
    todo_lists = getLists()
    for todo_list in todo_lists:
        if todo_list['id'] == list_id:
            entries = todo_list['entries']
            for entry in entries:
                if entry['id'] == entry_id:
                    entry['name'] = updated_entry['name']
                    entry['value'] = updated_entry['value']
                    updateLists(todo_lists)
                    return entry
            return 'entry not found'
    return 'list not found'
