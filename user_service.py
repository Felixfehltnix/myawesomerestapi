import json
import uuid


def getUsers():
    with open('inMemoryData/users.json', 'r') as usersJson:
        current_users = json.load(usersJson)
    return current_users


def updateUsers(users):
    with open('inMemoryData/users.json', 'w') as usersJson:
        json.dump(users, usersJson)


def createUser(user_object):
    users = getUsers()
    new_id = {'id': str(uuid.uuid4())}
    new_user = user_object | new_id
    users.append(new_user)
    updateUsers(users)
    return new_user


def deleteUser(user_id):
    users = getUsers()
    for user in users:
        if user['id'] == user_id:
            users.remove(user)
            updateUsers(users)
            return 'user deleted'
    return 'user not found'


def getUserById(user_id):
    users = getUsers()
    for user in users:
        if user['id'] == user_id:
            return user
    return None
