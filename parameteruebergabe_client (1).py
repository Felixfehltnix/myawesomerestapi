"""
#assert r.status_code == 405, 'GET-Anfrage an den POST-Endpunkt wurde mit falschem Status-Code beantwortet.'
#assert r.status_code == 200, 'POST-Anfrage wurde mit falschem Status-Code beantwortet.'
#assert r.json() == newList, 'POST-Anfrage liefert fehlerhafte Daten zurück.'

Zum testen der Endpoints, den passenden request einkommentieren und file compilieren.

"""

import urllib.parse

import requests

base_url = 'http://127.0.0.1:5000'

newUser = {
    'name': 'testUser',
}

newList = {
    'name': 'TestTitel',
    'description': 'this is my test TODO list'
}

newEntry = {
    'name': 'testEintrag',
    'value': 'remember yolo'
}

updatedEntry = {
    'name': 'Dieser Eintrag hat jetzt einen neuen Namen',
    'value': 'yoloMoloMarcoPolo'
}

# get = requests.get(urllib.parse.urljoin(base_url, '/list/1'))


#new user test
#new_user = requests.post(urllib.parse.urljoin(base_url, '/user'), data=newUser)
#print(new_user)

#new list test
#new_list = requests.post(urllib.parse.urljoin(base_url, '/list'), data=newList)
#print(new_list)


# new entry test
#new_entry = requests.post(urllib.parse.urljoin(
#    base_url, '/list/cd09bef6-9eb6-49f4-ad40-7b2e2aa0eb58/entry'),
#   data=newEntry)
#print(new_entry.content)


# delete list test
# delete_list = requests.delete(urllib.parse.urljoin(base_url, '/list/96f182af-6be2-4e60-9492-8c7ca6a85afc'))
# print(delete_list.content)


# delete entry test
# delete_entry = requests.delete(urllib.parse.urljoin(
#     base_url,
#     '/list/86eea6e0-4e5c-499f-8c95-5924b9e159b5/entry/d3b949a7-f02d-484d-90cd-31addbfdd202'
# ))
# print(delete_entry.content)


# delete user test
#delete_user = requests.delete(urllib.parse.urljoin(
#    base_url,
#    '/user/a0575b91-c8a0-4430-8d94-2a34ae5820f7'
#))
#print(delete_user.content, delete_user.status_code)


# update entry test
update_entry = requests.post(urllib.parse.urljoin(
    base_url,
    '/list/cd09bef6-9eb6-49f4-ad40-7b2e2aa0eb58/entry/402742ae-84c3-4d26-918c-804d517f16d9'
), data=updatedEntry)
print(update_entry)


# append user to entry test
#appended_entry = requests.post(urllib.parse.urljoin(
#    base_url,
#    '/user/6b676e17-65a4-4ffd-9145-9576d7aef5cf/402742ae-84c3-4d26-918c-804d517f16d9'
#))
#print(appended_entry.content)


